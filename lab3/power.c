#include <stdio.h>
#include "power.h"

double power(double a, double b) {

    int i;
    double result = 1;

    for (i = 0; i < b; i++) {
        result *= a;
    }

    return result;

}
