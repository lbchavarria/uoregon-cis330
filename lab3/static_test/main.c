#include <stdio.h>
#include "power.h"

int main(int argc, char* argv[]) {

    double a, b, p;
    
    a = 4;
    b = 3;
    p = power(a, b);

    printf("%f to the power of %f is %f\n", a, b, p);

    return 0;

}
