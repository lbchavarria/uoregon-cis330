#include <stdio.h>

void print5Triangle() {
    int triangle[5][9] = {
        {-1, -1, -1, -1, 0, -1, -1, -1, -1},
        {-1, -1, -1, 0, 1, 2, -1, -1, -1},
        {-1, -1, 0, 1, 2, 3, 4, -1, -1},
        {-1, 0, 1, 2, 3, 4, 5, 6, -1},
        {0, 1, 2, 3, 4, 5, 6, 7, 8}
    };

    int i;
    int j;

    for (i = 0; i < 5; i++) {
        for (j = 0; j < 9; j++) {
            if (triangle[i][j] >= 0) {
                printf("%d", triangle[i][j]);
            }
            else {
                printf(" ");
            }
        }
        printf("\n");
    }
}
