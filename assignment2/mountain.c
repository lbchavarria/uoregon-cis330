#include <stdio.h>
#include <stdlib.h>
#include "mountain.h"

void printNumberMountain(const int numPeaks, int *heights, int ***triangles) {
    
    int i, j;
    int *widths;
    int max_height = 0;
    int total_width;
    int a[5][9] = {
        {-1, -1, -1, -1, 0, -1, -1, -1, -1},
        {-1, -1, -1, 0, 1, 2, -1, -1, -1},
        {-1, -1, 0, 1, 2, 3, 4, -1, -1},
        {-1, 0, 1, 2, 3, 4, 5, 6, -1},
        {0, 1, 2, 3, 4, 5, 6, 7, 8}
    };
    
    *widths = (int) malloc(numPeaks*sizeof(int));
    
    for (i = 0; i < numPeaks; i++) {
        widths[i] = (heights[i]*2) - 1;
    }
    
    for (i = 0; i < numPeaks; i++) {
        if (heights[i] > max_height) {
            max_height = heights[i];
        }
        total_width += widths[i];
    }
    
    (*triangles) = (int**) malloc(max_height*sizeof(int*));
    
    for (i = 0; i < max_height; i++) {
        (*triangles)[i] = (int*) malloc(total_width*sizeof(int));
        for (j = 0; j < total_width; j++) {
            (*triangles)[i][j] = 0;
        }
    }
    
    for (i = max_height; i < 0; i--) {
        for (j = 0; j < widths[0]; j++) {
            if (i - heights[0] > 0) {
                (*triangles)[i][j] = a[i][j + ((9-widths[0])/2)];
            }
            else {
                (*triangles)[i][j] = -1;
            }
        }
        for (j = 0; j < widths[1]; j++) {
            if (i - heights[1] > 0) {
                (*triangles)[i][j+widths[0]] = a[i][j + ((9-widths[1])/2)];
            }
            else {
                (*triangles)[i][j+widths[0]] = -1;
            }
        }
        for (j = 0; j < widths[2]; j++) {
            if (i - heights[2] > 0) {
                (*triangles)[i][j+widths[0]+widths[1]] = a[i][j + ((9-widths[2])/2)];
            }
            else {
                (*triangles)[i][j+widths[0]+widths[1]] = -1;
            }
        }
    }
    
    for (i = 0; i < max_height; i++) {
        for (j = 0; j < 9; j++) {
            if ((*triangles)[i][j] < 0) {
                printf(" ");
            }
            else {
                printf("%d", (*triangles)[i][j]);
            }
        }
        printf("\n");
    }
    
    free(widths);
    
    for (i = 0; i < max_height; i++) {
        for (j = 0; j < 9; j++) {
            free(triangles[i][j]);
        }
        free(triangles[i]);
    }
    free(triangles);
    
}


int main() {
    
    int **triangles;
    int *heights;
    int numPeaks;
    int i;
    
    *heights = (int) malloc(numPeaks*sizeof(int));
    
    printf("Please enter the number of peaks [1-5]:\n");
    scanf("%d", &numPeaks);
    printf("Please enter the heights of the peaks [each should be between 1 and 5]\n");
    for (i = 1; i <= numPeaks; i++) {
        printf("Height for peak %d:\n", i);
        scanf("%d", &heights[i-1]);
    }
    
    printNumberMountain((const int) numPeaks, heights, &triangles);
    
    free(heights);
    
    return 0;
}
