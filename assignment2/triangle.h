#ifndef TRIANGLE_H_
#define TRIANGLE_H_

//Print a triangle of height 5
void print5Triangle();

//Print a triangle of the specified height containing the digits 0 - 9
void printNumberTriangle(const int height, int **triangle);

#endif //Triangle_H_
