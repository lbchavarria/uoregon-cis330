#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int getUserChoice(int current_sticks) {

    char* choice;
    int user_choice;
    
    choice = (char*)malloc(sizeof(char));

    // Prompt the user to pick up 1 to 3 sticks
    printf("Player 1: How many sticks do you take (1-3)?\n");
    scanf("%s", choice);
    //printf("%s\n", choice);
    
    if (*choice == 'q') {
        exit(1);
    }
    else {
        sscanf(choice, "%d", &user_choice);
    }

    // Check if number of sticks chosen is valid or possible
    while (user_choice < 1 || user_choice > 3 || user_choice > current_sticks) {
        if (user_choice < 1 || user_choice > 3) {
            printf("Enter a valid amount of sticks to take (1-3)\n");
        }
        if (user_choice > current_sticks) {
            printf("Not enough sticks\n");
        }
        scanf("%s", choice);
        if (*choice == 'q') {
            exit(1);
        }
        else {
            sscanf(choice, "%d", &user_choice);
        }
    }
    
    return user_choice;
}

int getComputerChoice(int current_sticks) {

    // get a pseudo-random integer between 1 and 3 (inclusive)
    int rand_choice = rand() % 3 + 1;

    if (rand_choice > current_sticks) {
        return current_sticks;
    }

    return rand_choice;
}

int main(int argc, char** argv) {
    int user, computer, number_sticks;

    srand (time(NULL));

    printf("Welcome to the game of sticks!\n");
    printf("To quit, type q anytime\n");
    printf("How many sticks are there on the table initially (10-100)?\n");
    scanf("%d", &number_sticks);

    while (number_sticks > 0) {
        user = getUserChoice(number_sticks);
        number_sticks -= user;
        printf("There are %d sticks on the board\n", number_sticks);
        if (number_sticks == 0) {
            printf("You Lose\n");
        }
        if (number_sticks != 0) {
            computer = getComputerChoice(number_sticks);
            printf("Computer selects %d\n", computer);
            number_sticks -= computer;
            printf("There are %d sticks left\n", number_sticks);
            if (number_sticks == 0) {
                printf("You Win\n");
            }
        }

    }
    
    return 0;
}
