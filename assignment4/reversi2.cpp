#include <iostream>
using namespace std;

class Reversi {
public:
    int static board_size;
    int static **board;
    void print_board(int board_size, int **board);
    void init_board(int board_size, int **board);
    bool is_legal(int **board, int row, int col);
    void update_score(int black_score, int white_score, int board_size, int **board);
    int update_black_score(int black_score, int board_size, int **board);
    int update_white_score(int white_score, int board_size, int **board);
    void print_score(int black_score, int white_score);
    void black_turn(int board_size, int **board);
    void white_move(int board_size, int **board);
    bool is_filled(int board_size, int **board);
    void flip_piece(int **board, int row, int col);
    void play_game(int board_size, int **board);
 
};


void Reversi::init_board(int board_size, int **board) {
    
    int i, j;
    
    board = new int*[board_size];
    
    for (i = 0; i < board_size; i++) {
        board[i] = new int[board_size];
        for (j = 0; j < board_size; j++) {
            board[i][j] = 0;
        }
    }
    
    /*
     for (i = 0; i <= board_size; i++) {
     (*board)[0][i] = i;
     (*board)[i][0] = i;
     }*/
    //(*board)[0][0] = -3;
    board[(board_size/2)-1][(board_size/2)-1] = -2;
    board[board_size/2][(board_size/2)-1] = -1;
    board[(board_size/2)-1][(board_size/2)] = -1;
    board[(board_size/2)][(board_size/2)] = -2;
}

void Reversi::print_board(int board_size, int **board) {
    
    int i, j;
    
    for (i = 0; i < board_size; i++) {
        for (j = 0; j < board_size; j++) {
            if (board[i][j] == 0) {
                cout << ". ";
            }
            else if (board[i][j] == -1) {
                cout << "b ";
            }
            else if (board[i][j] == -2) {
                cout << "w ";
            }
            /*else if (board[i][j] == -3) {
             printf("  ");
             }
             else {
             printf("%d ", board[i][j]);
             }*/
            if (j == board_size-1) {
                cout << endl;
            }
        }
    }
    cout << endl;
}

bool Reversi::is_legal(int **board, int row, int col) { //::FIXME::
    
    if (board[row][col] == -1) {
        if (board[row][col-1] == -2 || board[row][col+1] == -2 || board[row-1][col-1] == -2 || board[row+1][col-1] == -2 || board[row-1][col+1] == -2 || board[row+1][col+1] == -2 || board[row-1][col] == -2 || board[row+1][col] == -2) {
            return true;
        }
    }
    
    if (board[row][col] == -2) {
        if (board[row][col-1] == -1 || board[row][col+1] == -1 || board[row-1][col-1] == -1 || board[row+1][col-1] == -1 || board[row-1][col+1] == -1 || board[row+1][col+1] == -1 || board[row-1][col] == -1 || board[row+1][col] == -1) {
            return true;
        }
    }
    
    return false;
}

void Reversi::update_score(int black_score, int white_score, int board_size, int **board) {
    
    int i, j;
    black_score = 0;
    white_score = 0;
    
    for (i = 0; i < board_size; i++) {
        for (j = 0; j < board_size; j++) {
            if (board[i][j] == -1) {
                black_score += 1;
            }
            if (board[i][j] == -2) {
                white_score += 1;
            }
        }
    }
    //printf("%d %d\n", black_score, white_score);
    cout << black_score << " " << white_score << endl;
    
}

int Reversi::update_black_score(int black_score, int board_size, int **board) {
    
    int i, j;
    black_score = 0;
    //white_score = 0;
    cout << "black = 0" << endl;
    
    for (i = 0; i < board_size; i++) {
        cout << i << endl;
        for (j = 0; j < board_size; j++) {
            cout << j << endl;
            if (board[i][j] == -1) {
                cout << i << j << endl;
                black_score += 1;
                cout << black_score << endl;
            }
        }
    }
    cout << "done" << endl;
    
    return black_score;
    
}

int Reversi::update_white_score(int white_score, int board_size, int **board) {
    
    int i, j;
    //black_score = 0;
    white_score = 0;
    
    for (i = 0; i < board_size; i++) {
        for (j = 0; j < board_size; j++) {
            if (board[i][j] == -2) {
                white_score += 1;
            }
        }
    }
    
    return white_score;
    
}


void Reversi::print_score(int black_score, int white_score) {
    //printf("Black: %d          White: %d\n\n", black_score, white_score);
    cout << "Black: " << black_score << "          White: " << white_score << endl << endl;
}

void Reversi::black_turn(int board_size, int **board) { //::FIXME::
    
    int row, col, i;
    int flipnum;
    
    //printf("Black move (input row and column): ");
    cout << "Black move (input row and column): ";
    //scanf("%d %d", &row, &col);
    cin >> row >> col;
    
    board[row-1][col-1] = -1;
    
    if (!/*Reversi::*/is_legal(board, (row-1), (col-1))) {
        board[row-1][col-1] = 0;
        return;
    }
    
    //UP CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][col-1] == -2) {
            flipnum++;
        }
        if(board[(row-1)-i][col-1] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][col-1] = -1;
    }
    
    //DOWN CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][col-1] == -2) {
            flipnum++;
        }
        if(board[(row-1)+i][col-1] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][col-1] = -1;
    }
    
    //RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[row-1][(col-1)+i] == -2) {
            flipnum++;
        }
        if(board[row-1][(col-1)+i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[row-1][(col-1)+i] = -1;
    }
    
    //LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[row-1][(col-1)-i] == -2) {
            flipnum++;
        }
        if(board[row-1][(col-1)-i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[row-1][(col-1)-i] = -1;
    }
    
    //UP-RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][(col-1)+i] == -2) {
            flipnum++;
        }
        if(board[(row-1)-i][(col-1)+i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][(col-1)+i] = -1;
    }
    
    //UP-LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][(col-1)-i] == -2) {
            flipnum++;
        }
        if(board[(row-1)-i][(col-1)-i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][(col-1)-i] = -1;
    }
    
    //DOWN-RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][(col-1)+i] == -2) {
            flipnum++;
        }
        if(board[(row-1)+i][(col-1)+i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][(col-1)+i] = -1;
    }
    
    //DOWN-LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][(col-1)-i] == -2) {
            flipnum++;
        }
        if(board[(row-1)+i][(col-1)-i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][(col-1)-i] = -1;
    }
    
}

void Reversi::white_move(int board_size, int **board) { //::FIXME::
    
    int row, col, i;
    int flipnum;
    
    //printf("White move (input row and column): ");
    cout << "White move (input row and column";
    //scanf("%d %d", &row, &col);
    cin >> row >> col;
    
    board[row-1][col-1] = -2;
    
    if (!/*Reversi::*/is_legal(board, (row-1), (col-1))) {
        board[row-1][col-1] = 0;
        return;
    }
    
    //UP CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][col-1] == -2) {
            flipnum++;
        }
        if(board[(row-1)-i][col-1] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][col-1] = -1;
    }
    
    //DOWN CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][col-1] == -1) {
            flipnum++;
        }
        if(board[(row-1)+i][col-1] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][col-1] = -2;
    }
    
    //RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[row-1][(col-1)+i] == -1) {
            flipnum++;
        }
        if(board[row-1][(col-1)+i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[row-1][(col-1)+i] = -2;
    }
    
    //LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[row-1][(col-1)-i] == -1) {
            flipnum++;
        }
        if(board[row-1][(col-1)-i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[row-1][(col-1)-i] = -2;
    }
    
    //UP-RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][(col-1)+i] == -1) {
            flipnum++;
        }
        if(board[(row-1)-i][(col-1)+i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][(col-1)+i] = -2;
    }
    
    //UP-LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][(col-1)-i] == -1) {
            flipnum++;
        }
        if(board[(row-1)-i][(col-1)-i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][(col-1)-i] = -2;
    }
    
    //DOWN-RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][(col-1)+i] == -1) {
            flipnum++;
        }
        if(board[(row-1)+i][(col-1)+i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][(col-1)+i] = -2;
    }
    
    //DOWN-LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][(col-1)-i] == -1) {
            flipnum++;
        }
        if(board[(row-1)+i][(col-1)-i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][(col-1)-i] = -2;
    }
    
    
}


bool Reversi::is_filled(int board_size, int **board) {
    
    int i, j;
    
    for (i = 0; i < board_size; i++) {
        for (j = 0; j < board_size; j++) {
            if (board[i][j] == 0) {
                return false;
            }
        }
    }
    
    return true;
}

void Reversi::flip_piece(int **board, int row, int col) {
    
    if (board[row][col] == -1) {
        board[row][col] = -2;
    }
    else if (board[row][col] == -2) {
        board[row][col] = -1;
    }
    
}

void Reversi::play_game(int board_size, int **board) {
    
    int black_score, white_score;
    bool game = true;
    
    /*Reversi::*/init_board(board_size, board);
    cout << "Board created" << endl;
    /*Reversi::*/black_score = update_black_score(black_score, board_size, board);
    cout << "Black score" << endl;
    /*Reversi::*/white_score = update_white_score(white_score, board_size, board);
    cout << "White score" << endl;
    
    /*Reversi::*/print_score(black_score, white_score);
    /*Reversi::*/print_board(board_size, board);
    
    while (game) {
        /*Reversi::*/black_turn(board_size ,board);
        //update_score(black_score, white_score, board_size, board);
        /*Reversi::*/black_score = update_black_score(black_score, board_size, board);
        /*Reversi::*/white_score = update_white_score(white_score, board_size, board);
        /*Reversi::*/print_score(black_score, white_score);
        /*Reversi::*/print_board(board_size, board);
        if (/*Reversi::*/is_filled(board_size, board)) {
            break;
        }
        
        /*Reversi::*/white_move(board_size, board);
        //update_score(black_score, white_score, board_size, board);
        /*Reversi::*/black_score = update_black_score(black_score, board_size, board);
        /*Reversi::*/white_score = update_white_score(white_score, board_size, board);
        /*Reversi::*/print_score(black_score, white_score);
        /*Reversi::*/print_board(board_size, board);
        if (/*Reversi::*/is_filled(board_size, board)) {
            break;
        }
    }
    
    
}


int main() {
    
    Reversi Reversi;

    //int **board;
    //int board_size;
    
    //printf("Please enter size of board (max size 12, even numbers only): ");
    cout << "Please enter size of board (max size 12, even numbers only): ";
    cin >> Reversi::board_size;
    
    cout << "Start Game" << endl;
    
    Reversi::play_game(Reversi::board_size, Reversi::board);
    
//    delete[][] {board};
    return 0;
}
