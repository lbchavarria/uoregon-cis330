#ifndef REVERSI_H_
#define REVERSI_H_

#include <stdbool.h>


typedef enum cell_type {
    Black, White, Empty
} cell_type;

void init_board(int board_size, int ***board);

void play_game(int board_size, int **board);

bool is_filled(int board_size, int **board);

bool is_legal(int **board, int row, int col);

void black_turn(int board_size, int **board);

void white_turn(int **board);

void print_board(int board_size, int **board);

void print_score(int black_score, int white_score);

void update_score(int black_score, int white_score, int board_size, int **board);

int update_black_score(int black_score, int board_size, int **board);

int update_white_score(int white_score, int board_size, int **board);

void end_game();

void flip_piece(int **board, int row, int col);




#endif
