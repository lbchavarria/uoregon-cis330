#include <stdio.h>
#include <stdlib.h>
#include "reversi.h"

void init_board(int board_size, int ***board) {
    
    int i, j;
    
    (*board) = (int**) malloc((board_size)*sizeof(int*));
    
    for (i = 0; i < board_size; i++) {
        (*board)[i] = (int*) malloc((board_size)*sizeof(int));
        for (j = 0; j < board_size; j++) {
            (*board)[i][j] = 0;
        }
    }
    
    /*
    for (i = 0; i <= board_size; i++) {
        (*board)[0][i] = i;
        (*board)[i][0] = i;
    }*/
    //(*board)[0][0] = -3;
    (*board)[(board_size/2)-1][(board_size/2)-1] = -2;
    (*board)[board_size/2][(board_size/2)-1] = -1;
    (*board)[(board_size/2)-1][(board_size/2)] = -1;
    (*board)[(board_size/2)][(board_size/2)] = -2;
}

void print_board(int board_size, int **board) {
    
    int i, j;
    
    for (i = 0; i < board_size; i++) {
        for (j = 0; j < board_size; j++) {
            if (board[i][j] == 0) {
                printf(". ");
            }
            else if (board[i][j] == -1) {
                printf("b ");
            }
            else if (board[i][j] == -2) {
                printf("w ");
            }
            /*else if (board[i][j] == -3) {
                printf("  ");
            }
            else {
                printf("%d ", board[i][j]);
            }*/
            if (j == board_size-1) {
                printf("\n");
            }
        }
    }
    printf("\n");
}

void update_score(int black_score, int white_score, int board_size, int **board) {
    
    int i, j;
    black_score = 0;
    white_score = 0;
    
    for (i = 0; i < board_size; i++) {
        for (j = 0; j < board_size; j++) {
            if (board[i][j] == -1) {
                black_score += 1;
            }
            if (board[i][j] == -2) {
                white_score += 1;
            }
        }
    }
    printf("%d %d\n", black_score, white_score);
    
}

int update_black_score(int black_score, int board_size, int **board) {
    
    int i, j;
    black_score = 0;
    //white_score = 0;
    
    for (i = 0; i < board_size; i++) {
        for (j = 0; j < board_size; j++) {
            if (board[i][j] == -1) {
                black_score += 1;
            }
        }
    }

    return black_score;
    
}

int update_white_score(int white_score, int board_size, int **board) {
    
    int i, j;
    //black_score = 0;
    white_score = 0;
    
    for (i = 0; i < board_size; i++) {
        for (j = 0; j < board_size; j++) {
            if (board[i][j] == -2) {
                white_score += 1;
            }
        }
    }

    return white_score;
    
}


void print_score(int black_score, int white_score) {
    printf("Black: %d          White: %d\n\n", black_score, white_score);
}

void black_turn(int board_size, int **board) { //::FIXME::
    
    int row, col, i;
    int flipnum;
    
    printf("Black move (input row and column): ");
    scanf("%d %d", &row, &col);
    
    board[row-1][col-1] = -1;
    
    if (!is_legal(board, (row-1), (col-1))) {
        board[row-1][col-1] = 0;
    }
    
    //UP CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][col-1] == -2) {
            flipnum++;
        }
        if(board[(row-1)-i][col-1] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][col-1] = -1;
    }

    //DOWN CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][col-1] == -2) {
            flipnum++;
        }
        if(board[(row-1)+i][col-1] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][col-1] = -1;
    }
    
    //RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[row-1][(col-1)+i] == -2) {
            flipnum++;
        }
        if(board[row-1][(col-1)+i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[row-1][(col-1)+i] = -1;
    }
    
    //LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[row-1][(col-1)-i] == -2) {
            flipnum++;
        }
        if(board[row-1][(col-1)-i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[row-1][(col-1)-i] = -1;
    }
    
    //UP-RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][(col-1)+i] == -2) {
            flipnum++;
        }
        if(board[(row-1)-i][(col-1)+i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][(col-1)+i] = -1;
    }
    
    //UP-LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][(col-1)-i] == -2) {
            flipnum++;
        }
        if(board[(row-1)-i][(col-1)-i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][(col-1)-i] = -1;
    }
    
    //DOWN-RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][(col-1)+i] == -2) {
            flipnum++;
        }
        if(board[(row-1)+i][(col-1)+i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][(col-1)+i] = -1;
    }
    
    //DOWN-LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][(col-1)-i] == -2) {
            flipnum++;
        }
        if(board[(row-1)+i][(col-1)-i] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][(col-1)-i] = -1;
    }
    
}

void white_move(int **board) { //::FIXME::
    
    int row, col;
    
    printf("White move (input row and column): ");
    scanf("%d %d", &row, &col);
    
    if (is_legal(board, (row-1), (col-1))) {
        board[row-1][col-1] = -2;
    }
    
    //UP CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][col-1] == -2) {
            flipnum++;
        }
        if(board[(row-1)-i][col-1] == -1) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][col-1] = -1;
    }
    
    //DOWN CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][col-1] == -1) {
            flipnum++;
        }
        if(board[(row-1)+i][col-1] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][col-1] = -2;
    }
    
    //RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[row-1][(col-1)+i] == -1) {
            flipnum++;
        }
        if(board[row-1][(col-1)+i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[row-1][(col-1)+i] = -2;
    }
    
    //LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[row-1][(col-1)-i] == -1) {
            flipnum++;
        }
        if(board[row-1][(col-1)-i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[row-1][(col-1)-i] = -2;
    }
    
    //UP-RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][(col-1)+i] == -1) {
            flipnum++;
        }
        if(board[(row-1)-i][(col-1)+i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][(col-1)+i] = -2;
    }
    
    //UP-LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)-i][(col-1)-i] == -1) {
            flipnum++;
        }
        if(board[(row-1)-i][(col-1)-i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)-i][(col-1)-i] = -2;
    }
    
    //DOWN-RIGHT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][(col-1)+i] == -1) {
            flipnum++;
        }
        if(board[(row-1)+i][(col-1)+i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][(col-1)+i] = -2;
    }
    
    //DOWN-LEFT CHECK
    flipnum = 0;
    for (i = 1; i < board_size-row; i++) {
        if (board[(row-1)+i][(col-1)-i] == -1) {
            flipnum++;
        }
        if(board[(row-1)+i][(col-1)-i] == -2) {
            break;
        }
    }
    
    for (i = 1; i <= flipnum; i++) {
        board[(row-1)+i][(col-1)-i] = -2;
    }

    
}

bool is_legal(int **board, int row, int col) { //::FIXME::
    
    if (board[row][col] == -1) {
        if (board[row][col-1] == -2 || board[row][col+1] == -2 || board[row-1][col-1] == -2 || board[row+1][col-1] == -2 || board[row-1][col+1] == -2 || board[row+1][col+1] == -2 || board[row-1][col] == -2 || board[row+1][col] == -2) {
            return true;
        }
    }
    
    if (board[row][col] == -2) {
        if (board[row][col-1] == -1 || board[row][col+1] == -1 || board[row-1][col-1] == -1 || board[row+1][col-1] == -1 || board[row-1][col+1] == -1 || board[row+1][col+1] == -1 || board[row-1][col] == -1 || board[row+1][col] == -1) {
            return true;
        }
    }
    
    return false;
}

bool is_filled(int board_size, int **board) {
    
    int i, j;
    
    for (i = 0; i < board_size; i++) {
        for (j = 0; j < board_size; j++) {
            if (board[i][j] == 0) {
                return false;
            }
        }
    }
    
    return true;
}

void flip_piece(int **board, int row, int col) {
    
    if (board[row][col] == -1) {
        board[row][col] = -2;
    }
    else if (board[row][col] == -2) {
        board[row][col] = -1;
    }
    
}

void play_game(int board_size, int **board) {
    
    int black_score, white_score;
    bool game = true;
    
    init_board(board_size, &board);
    black_score = update_black_score(black_score, board_size, board);
    white_score = update_white_score(white_score, board_size, board);
    
    print_score(black_score, white_score);
    print_board(board_size, board);
    
    while (game) {
        black_turn(board_size ,board);
        //update_score(black_score, white_score, board_size, board);
        black_score = update_black_score(black_score, board_size, board);
        white_score = update_white_score(white_score, board_size, board);
        print_score(black_score, white_score);
        print_board(board_size, board);
        if (is_filled(board_size, board)) {
            break;
        }
        
        white_move(board);
        //update_score(black_score, white_score, board_size, board);
        black_score = update_black_score(black_score, board_size, board);
        white_score = update_white_score(white_score, board_size, board);
        print_score(black_score, white_score);
        print_board(board_size, board);
        if (is_filled(board_size, board)) {
            break;
        }
    }
    
    
}

int main() {
    
    int board_size;
    int **board;

    printf("Please enter size of board (max size 12, even numbers only): ");
    scanf("%d", &board_size);

    printf("Start game\n");
    
    play_game(board_size, board);
   
 
    return 0;
}
